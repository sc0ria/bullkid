function clickCalc(){
	var txtSexo = document.getElementById("sexo");
	var sexo = txtSexo.value;

	var txtEdad = document.getElementById("edad");
	var edad = txtEdad.value;

	var txtAltura = document.getElementById("altura");
	var altura = txtAltura.value;

	var txtPeso = document.getElementById("peso");
	var peso = txtPeso.value;

	var txtActividad = document.getElementById("actividad");
	var actividad = txtActividad.value;

	var txtObjetivo = document.getElementById("objetivo");
	var objetivo = txtObjetivo.value;

	let BMR=0;
	let actBMR=0;
	let IMC='';
	let BMI=0;
	let caloriasDiarias1=0;
	let caloriasDiarias2=0;

	let prot1=0;
	let prot2=0;
	let gras1=0;
	let gras2=0;
	let carbs1=0;
	let carbs2=0;

	var proceed=0;

	if (sexo === "" || edad < 10 || edad > 80 || edad === "" || altura < 100 || altura > 230 || altura === ""
		|| peso < 25 || peso > 150 || peso === "" || actividad === ""){
		proceed=0;
	} else if (sexo === 'hombre'){

		//Calculo BMR
		BMR = 9.99 * peso + 6.25 * altura - 4.92 * edad + 5; 

		//Calculo Calorias diarias por actividad
		switch (actividad){
			case 'act1': actBMR = BMR * 1.2; break;
			case 'act2': actBMR = BMR * 1.37; break;
			case 'act3': actBMR = BMR * 1.55; break;
			case 'act4': actBMR = BMR * 1.73; break;
			case 'act5': actBMR = BMR * 1.9; break;
		}

		//Calculo BMI
		BMI= (peso / (altura * altura)) * 10000; 
		BMI= BMI.toFixed(2);

		//Calculo IMC
		if (BMI < 18.5){
			IMC = 'Bajo Peso';
		} else if (BMI < 25){
			IMC = 'Peso Intermedio';
		} else if (BMI < 30){
			IMC = 'Sobrepeso';
		} else if (BMI >30){
			IMC= 'Obesidad';
		}

		//Calculo calorias diarias por objetivo margen 1
		switch (objetivo){
			case 'obj1': caloriasDiarias1=actBMR*0.8; break;
			case 'obj2': caloriasDiarias1=actBMR; break;
			case 'obj3': caloriasDiarias1=actBMR*1.2; break;
		}

		//Calculo calorias diarias por objetivo margen 2
		caloriasDiarias2=actBMR;

		//Calculo de macronutrientes
		prot1 = caloriasDiarias1 *  0.2/4;
		prot2 = caloriasDiarias1 *  0.25/4;

		gras1 = caloriasDiarias1 * 0.70/9;
		gras2 = caloriasDiarias1 * 0.75/9;

		carbs1 = caloriasDiarias1 * 0.05/4;


		proceed=1;

		//Redondeo de datos

		caloriasDiarias1=Math.round(caloriasDiarias1);
		caloriasDiarias2=Math.round(caloriasDiarias2);
		prot1=Math.round(prot1);
		prot2=Math.round(prot2);
		gras1=Math.round(gras1);
		gras2=Math.round(gras2);
		carbs1=Math.round(carbs1);
	
	} else if (sexo === 'mujer'){
		
		//Calculo BMR
		BMR = 9.99 * peso + 6.25 * altura - 4.92 * edad - 161; 

		//Calculo Calorias diarias por actividad
		switch (actividad){
			case 'act1': actBMR = BMR * 1.2; break;
			case 'act2': actBMR = BMR * 1.37; break;
			case 'act3': actBMR = BMR * 1.55; break;
			case 'act4': actBMR = BMR * 1.73; break;
			case 'act5': actBMR = BMR * 1.9; break;
		}

		//Calculo BMI
		BMI= (peso / (altura * altura)) * 10000; 
		BMI= BMI.toFixed(2);

		//Calculo IMC
		if (BMI < 18.5){
			IMC = 'Bajo Peso';
		} else if (BMI < 25){
			IMC = 'Peso Intermedio';
		} else if (BMI < 30){
			IMC = 'Sobrepeso';
		} else if (BMI >30){
			IMC= 'Obesidad';
		}

		//Calculo calorias diarias por objetivo margen 1
		switch (objetivo){
			case 'obj1': caloriasDiarias1=actBMR*0.8; break;
			case 'obj2': caloriasDiarias1=actBMR; break;
			case 'obj3': caloriasDiarias1=actBMR*1.2; break;
		}

		//Calculo calorias diarias por objetivo margen 2
		caloriasDiarias2=actBMR;

		//Calculo de macronutrientes
		prot1 = caloriasDiarias1 *  0.2/4;
		prot2 = caloriasDiarias1 *  0.25/4;

		gras1 = caloriasDiarias1 * 0.70/9;
		gras2 = caloriasDiarias1 * 0.75/9;

		carbs1 = caloriasDiarias1 * 0.05/4;


		proceed=1;

		//Redondeo de datos
		caloriasDiarias1=Math.round(caloriasDiarias1);
		caloriasDiarias2=Math.round(caloriasDiarias2);
		prot1=Math.round(prot1);
		prot2=Math.round(prot2);
		gras1=Math.round(gras1);
		gras2=Math.round(gras2);
		carbs1=Math.round(carbs1);
	


	}

	//Mostrando resultados
	if (proceed==0){
		alert("Verifique si los datos ingresados estan escritos correctamente.");
	} else if (proceed==1){
		document.getElementById("resultados").innerHTML = "Resultados";
		document.getElementById("t1").innerHTML = "Calorías Diarias";

		var p1 = document.getElementById("p1");

		if (objetivo === "obj1"){
			p1.innerHTML = "Las calorías que debes consumir diariamente son entre " + caloriasDiarias1 + " y " + caloriasDiarias2 + " calorías.";
			document.getElementById("rut").src="img/img1.jpg";
		} else if (objetivo === "obj2"){
			p1.innerHTML = "Las calorías que debes consumir diariamente son " + caloriasDiarias1 + " calorías.";
			document.getElementById("rut").src="img/img2.jpg";
		} else if (objetivo === "obj3"){
			p1.innerHTML = "Las calorías que debes consumir diariamente son entre " + caloriasDiarias2 + " y " + caloriasDiarias1 + " calorías.";
			document.getElementById("rut").src="img/img3.jpg";
		}

	
		var p2 = document.getElementById("p2");
		p2.innerHTML = "Su IMC es de: " + BMI + ". Clasificación: " + IMC;

		var t2 = document.getElementById("t2"); 
		t2.innerHTML = "Macronutrientes Diarios en Gramos";

		var t3 = document.getElementById("t3");
		t3.innerHTML = "Para 20-25% de Proteínas, 70-75% de Grasas y 5% de Carbohidratos (dieta cetogénica)";

		var p3 = document.getElementById("p3");
		p3.innerHTML= "Proteínas: " + prot1 + "g-" + prot2 + "g";

		var p4 = document.getElementById("p4");
		p4.innerHTML= "Grasas: " + gras1 + "g-" + gras2 + "g";

		var p5 = document.getElementById("p5");
		p5.innerHTML= "Carbohidratos: " + carbs1 + "g";

		var t4 = document.getElementById("t4");
		t4.innerHTML = "Rutina de Ejercicios";

	

	}


}